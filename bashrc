# .bashrc
# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

. ~/scripts/vcs.sh 2> /dev/null

export PATH=$PATH:~/.local/bin:~/.gem/ruby/bin:~/programs/minishift/preferred

function mkd {
    mkdir -p $1
    cd $1
}

function clip {
    cat $1 | xsel
}


function pubbuild {
	publican build --langs=en-US --formats=html-single --config $* && notify-send "Building $(basename $PWD)" complete || notify-send "Building $(basename $PWD)" failed
}

function calendar {
	firefox "$(python3 ~/scripts/calendar.py $*)"
}

function docsearch {
	find -P -name \*.adoc -exec grep --colour=always -Hns "$@" '{}' ';'
}

function vless {
	vim - -c 'au VimEnter * set nomod' -c 'noremap q :q<CR>'
}

function vlesshl {
	vless -c "set filetype=$@"
}

function buildwatcher {
	case "$1" in
		"asciidoctor")
			command="asciidoctor master.adoc"
			;;
		"ccutil")
			command="ccutil compile --lang en-US"
			;;
		"sh")
			command="./buildGuide.sh"
			;;
		*)
			echo "Usage: buildwatcher asciidoctor|ccutil|sh"
			exit 2
	esac

	while :; do
		inotifywait $(find -L -name \*.adoc) -e MODIFY &>/dev/null
		for directory in $(dirname $(realpath $(find -name master.adoc))); do
			echo "Building ${directory}..." >&2
			pushd $directory &>/dev/null
			$command
			popd &>/dev/null
		done
	done
}

function gsync {
    if ! git rev-parse &>/dev/null; then
        echo "Not a Git repository, quitting..." >&2
        return 1
    fi

    if git remote | grep -q upstream; then
        upstream="upstream"
    elif git remote | grep -q origin; then
        upstream="origin"
    else
        echo "Remote 'upstream' or 'origin' not found, quitting..." >&2
        return 2
    fi

    branch="$(git rev-parse --abbrev-ref HEAD)"

    git fetch $upstream
    git reset --hard $upstream/$branch
    git push
}

# User specific aliases and functions
alias ..='cd ..'
alias cls='printf "\033c"'
alias compare='~/scripts/compare_archives.sh'
alias editrc='vim ~/.bashrc; . ~/.bashrc'
alias editvim='vim ~/.config/nvim/init.vim'
alias setend='xmodmap -e "keycode 118 = End NoSymbol End"'
alias setinsert='xmodmap -e "keycode 118 = Insert NoSymbol Insert"'
alias la='ls -lA'
alias lr='ls -R'
alias lsd='ls --group-directories-first -x'
alias search='grep -Hnis'
alias disk-sleep='sudo hdparm -Y /dev/sd{a,b}'
alias rsearch='grep -RHnis'
alias mount-server='/home/tradej/scripts/mount-server.sh'
alias vim='/usr/bin/nvim'

# Programming
alias batch-dl='while :; do read -p "Enter URL: " DL; wget "$DL"; DL=""; done'
alias delswp='find -iname "*.swp" -delete'
alias find-python-files='find -type f -exec file \{\} \; | grep -i python | sed -e "s/:.*//" | grep -v ".*.py[co]$" | sort'
alias py='python'
alias py3='python3'
alias p3pip='python3-pip'
alias clean-python='rm -rf dist/ build/ *.egg-info/'
alias mck='if [ "$MOCKCHROOT" == "" ]; then read -p "Chroot: " MOCKCHROOT; fi; mock -r $MOCKCHROOT'
alias mck-copy-result='if [ "$MOCKCHROOT" == "" ]; then read -p "Chroot: " MOCKCHROOT; fi; cp "/var/lib/mock/$MOCKCHROOT/result" -rt'
alias mck-switch='read -p "Chroot: " MOCKCHROOT'
alias retval='echo $?'
alias run-tests='./setup.py test && notify-send "Tests successful" || notify-send "Tests failed"'

# DevAssistant
alias dap-build='git clean-all; rm *.dap; delswp; da twk dap pack; da pkg lint *.dap'
alias dap-reinstall='yes | da pkg uninstall $(basename $PWD | sed -e "s/dap-//"); da pkg install ./*.dap'

# Fedora
alias get-sources='spectool -g *.spec'
alias pipun='yes | pip uninstall'
alias prep='fedpkg prep'
alias query-rawhide='repoquery --enablerepo=rawhide'
alias query-src-rawhide='repoquery --enablerepo=rawhide --archlist=src'
alias spec='vim *.spec'
alias srpm='dnf repoquery -q --qf "%{SOURCERPM}"'

# RH
alias clip-status='source ~/scripts/clip-status-report.sh'
alias etherpad='firefox "$(python3 ~/scripts/etherpad.py)"'
alias etherpad-next='firefox "$(python3 ~/scripts/etherpad.py 1)"'
alias status='~/scripts/status.py'

# Content Services
alias build='~/scripts/build.sh'
alias showdoc='firefox $(find -name \*.html)'
alias buildwatcher-docbook='while :; do inotifywait en-US/*.xml -e MODIFY; rm -rf tmp* 2> /dev/null; for config in b{r,p}ms; do test -f $config.cfg && publican build --config $config.cfg --langs en-US --formats html-single && mv tmp{,_$config}; done; done'
alias buildwatcher-asciidoc='while :; do inotifywait $(find -L -name \*.adoc) -e MODIFY; rm $(find -name master.html) 2> /dev/null; for product in !(topics)/; do test -d $product || continue; echo Building $product; pushd $product &> /dev/null; asciidoctor master.adoc; popd; done; done'
alias buildwatcher-ccutil='while :; do inotifywait $(find -L -name \*.adoc) -e MODIFY; rm $(find -name master.html) 2> /dev/null; for product in !(topics)/; do test -d $product || continue; echo Building $product; pushd $product &> /dev/null; ccutil compile --lang en-US; popd; done; done'
alias cleantmp='rm -rf $(find -type d -name tmp)'
alias abuild='asciidoctor master.adoc'
alias cbuild='ccutil compile --lang en-US'

# RHOAR
alias rbuild='pushd $(git rev-parse --show-toplevel) &>/dev/null; ./scripts/build_guides.sh; popd &>/dev/null'
alias rclean='pushd $(git rev-parse --show-toplevel) >/dev/null && git clean -d -f -f && rm -rf html && popd &>/dev/null'
alias rserver='sudo CICO_LOCAL=true ./cico_build_deploy.sh && sudo docker run -p 80:8080 -it appdev-documentation-deploy:latest'
alias rshowdoc='pushd $(git rev-parse --show-toplevel)/html &>/dev/null; firefox *.html; popd'
alias rvalidate='pushd $(git rev-parse --show-toplevel) &>/dev/null; ./scripts/validate_guides.sh; popd &>/dev/null'

# WildFly Swarm
alias mbuild='pushd $(git rev-parse --show-toplevel)/docs &>/dev/null; mvn generate-resources -DskipTests; popd &>/dev/null'
alias mpbuild='pushd $(git rev-parse --show-toplevel)/docs &>/dev/null; mvn generate-resources -DskipTests -Dswarm.product.build -s $(git rev-parse --show-toplevel)/settings.xml; popd &>/dev/null'

# Git
alias gamend='git commit --amend -m"`git log -1 --pretty=%B`"'
alias gc='git checkout'
alias gclean='git clean -d -f -f "$(git rev-parse --show-toplevel)"'
alias gco='git commit -m'
alias gdiff='git diff'
alias gdiffw='git diff --word-diff'
alias gdiffrm='git fetch; git diff $(git rev-parse --abbrev-ref --symbolic-full-name @{u})'
alias gdifft='git difftool -d'
alias gfa='git fetch --all'
alias ginit='git init && git add * && git commit -a -m "Initial commit"'
alias gup='git fetch --all && git rebase upstream/$(git rev-parse --abbrev-ref HEAD)'
alias gprune='git remote prune origin && git branch -D $(git branch --merged | grep -v "\*\|preview\|stage\|l10n")'
alias gpr='git pull --rebase'
alias greset='git reset --hard $(git rev-parse --abbrev-ref $(git rev-parse --abbrev-ref HEAD)@{upstream})'

alias grmbackup='git branch -d $(git branch | grep -v "\*" | grep "\-backup")'

alias gst='git status'
alias qgit='QT_SCREEN_SCALE_FACTORS=1\;1.5\;1 /usr/bin/qgit --all 2> /dev/null &'
alias wtf='git reset --hard'

export RPM_PACKAGER='Tomas Radej <tradej@redhat.com>'

export GPG_TTY=$(tty)

