#!/usr/bin/python3

from bs4 import BeautifulSoup
import requests
import sys
import subprocess

SHOP_URL = 'https://falba.tech/product-category/keyboard-parts/ergodox-parts/cases-ergodox/'

class Product():
    def __init__(self, title, price, available):
        self.title = title
        self.price = price
        self.available = available

    def pretty_available(self):
        return 'Yes' if self.available else 'No'


def get_content():
    session = requests.Session()
    session.trust_env = False
    response = session.get(SHOP_URL, headers={'User-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)'})
    if not response.ok:
        raise Exception('Could not get page content')
    else:
        return response.text

def get_content_with_curl(suffix=''):
    result = subprocess.check_output(['curl', '-H', 'User-agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6)', SHOP_URL+suffix])
    return result

def get_products(page_content):
    soup = BeautifulSoup(page_content, 'html.parser')
    result = []
    for tag in soup.find_all('li', class_='product'):
        if not 'fullhand' in tag.h3.string.lower():
            continue
        title = tag.h3.string
        tag.find('span', class_='amount').span.extract()
        price = float(tag.find('span', class_='amount').get_text())
        available = not bool(tag.find('span', class_='out-of-stock'))
        result.append(Product(title, price, available))
    return result

def get_number_of_pages(page_content):
    soup = BeautifulSoup(page_content, 'html.parser')
    return len(soup.find_all('a', class_='page-numbers'))

def product_sort(product):
    preferences = {'b-stock': 1, 'oak': 10, 'bamboo': 5}
    result = 0
    for string in preferences.keys():
        if string in product.title.lower():
            result =+ preferences[string]
    return result

def print_products(products):
    for product in sorted(products, key=product_sort, reverse=True):
        print('{title}: {price} ({available})'.format(title=product.title,
                                                         price=product.price,
                                                         available=product.pretty_available()))

def run() -> int:
    print('Processing first page', file=sys.stderr)
    first_page = get_content_with_curl()
    products = get_products(first_page)

    number_of_pages = get_number_of_pages(first_page)
    if number_of_pages > 1:
        for i in range(2, number_of_pages+1):
            print('Processing page ' + str(i), file=sys.stderr)
            products.extend(get_products(get_content_with_curl('page/' + str(i))))

    available_products = [p for p in products if p.available]
    print_products(available_products)
    return 0

if __name__ == '__main__':
    sys.exit(run())

