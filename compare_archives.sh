#!/usr/bin/bash

if [ $# -lt 2 ]; then
    echo 'Two arguments expected'
    exit
fi

old=$1
new=$2

tmp_old=/tmp/tmp$$.old.list
dtrx -l $old | sort > $tmp_old

tmp_new=/tmp/tmp$$.new.list
dtrx -l $new | sort > $tmp_new

meld $tmp_old $tmp_new 2> /dev/null

rm $tmp_old $tmp_new
