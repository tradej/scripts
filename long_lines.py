
import os

for root, dirs, files in os.walk('.'):

    for f in files:
        if not f.endswith('.py'):
            continue
        fname = os.path.join(root, f)
        with open(fname, 'r') as fh:
            try:
                lines = fh.readlines()
            except:
                continue

            counter = 0
            for l in lines:
                counter += 1
                if len(l) > 99:
                    print('{fn} ({n}): {l}'.format(fn=fname, n=counter, l=l))


