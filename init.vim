
let mapleader = ","

map <Leader>q :qa<CR>

" Movement
map <c-j> gj
map <c-k> gk
map <c-l> gl
map <c-h> gh

" Movement - Windows
map <Leader>j <c-w>j
map <Leader>k <c-w>k
map <Leader>l <c-w>l
map <Leader>h <c-w>h

" Movement - Tabs
map <Leader>M <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>

" Movement - Other
map <Leader>o <c-W>gf

" Search results highlighting
noremap <C-n> :nohl<CR>

" Toggle Wrapping
function ToggleWrap()
    if (&wrap == 1)
        set nowrap nolinebreak
    else
        set wrap linebreak
    endif
endfunction
map <Leader>w :call ToggleWrap()<CR>

" Indentation
vnoremap < <gv
vnoremap > >gv

" Tabs
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab

" Show whitespace
" MUST be inserted BEFORE the colorscheme command
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()

" Color scheme
set t_Co=256

" Enable syntax highlighting
" You need to reload this file for the change to apply
filetype off
filetype plugin indent on
syntax on

" Interface
set number
set nowrap
set colorcolumn=80,100
highlight ColorColumn ctermbg=LightGray

" Spell checking
function ToggleSpellChecking()
    if (&spell == 1)
        set nospell
    else
        set spell
    endif
endfunction
map <Leader>s :call ToggleSpellChecking()<CR>
set nospell

" History
set history=700
set undolevels=700

" Make search case insensitive
set hlsearch
set incsearch
set ignorecase
set smartcase

" Spell checking
set spelllang=en_us
set nospell

" Plugins
call plug#begin('~/.local/share/nvim/plugged')
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'davidhalter/jedi-vim'
call plug#end()

" NERDTree
nmap <Leader>v :NERDTreeToggle<CR>
vmap <Leader>v :NERDTreeToggle<CR>

let g:NERDTreeWinSize = 50

" Airline
let g:airline#extensions#hunks#enabled = 0
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#wordcount#enabled = 0
let g:airline_powerline_fonts = 1

" Jedi
let g:jedi#show_call_signatures = 0


